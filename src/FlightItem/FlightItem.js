import React, { Component } from 'react';
import './FlightItem.css';
import { createDateTime } from '../utils/tools.js';


class FlightItem extends Component {
    constructor ( props ) {
        super(props);
        this.state = {

        };
        [].forEach( m => {
            this[m] = this[m].bind(this);
        });
    }
    render () {
        return (
            <article className="flight-item">
                <div className="flight-details">
                    <div>From:</div>
                    <div><b>{ this.props.data.cityFrom }, <span>{ createDateTime(this.props.data.dTime) }</span></b></div>
                    <br/>
                    <div>To:</div>
                    <div><b>{ this.props.data.cityTo }, <span>{ createDateTime(this.props.data.aTime) }</span></b></div>
                </div>
                <div className="flight-price">
                    { this.props.data.price } &euro;
                </div>
            </article>
        );
    }
}

export default FlightItem;
