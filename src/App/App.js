import React, { Component } from 'react';
import './App.css';
import FlightSelector from '../FlightSelector/FlightSelector.js';
import FlightResults from '../FlightResults/FlightResults.js';
import Loader from '../Loader/Loader.js';
import API from '../io/api.js';

let api = API.getInstance();

class App extends Component {
    constructor ( props ) {
        super(props);
        this.state = {
            flights: [],
            loading: false,
            firstTime: true
        };
        ['search'].forEach( m => {
            this[m] = this[m].bind(this);
        });
    }
    search ( flightSetup ) {
        this.setState({
            loading: true,
            firstTime: false
        });
        api.findFlights(flightSetup).then( flights => {
            this.setState({
                flights: flights.data,
                loading: false
            });
        });
    }
    render() {
        return (
            <div>
                <div id="topbar">
                    <FlightSelector onSearch={ this.search } oneWay={ true } />
                </div>
                { ! this.state.firstTime && (
                    <div id="flight-results">
                        { this.state.loading ?
                            ( <Loader /> )
                                :
                            ( <FlightResults flights={ this.state.flights } /> )
                        }
                    </div>
                )}
            </div>
        );
    }
}

export default App;
