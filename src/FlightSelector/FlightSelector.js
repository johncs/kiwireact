import React, { Component } from 'react';
import './FlightSelector.css';
import AirportSelector from '../AirportSelector/AirportSelector.js';
import { formatDate, getToday, formatDateForInput } from '../utils/tools.js';


class FlightSelector extends Component {
    constructor ( props ) {
        super(props);
        this.state = {
            fromAirportID: null,
            toAirportID: null,
            departDate: getToday(),
            returnDate: "",
            oneWay: this.props.oneWay || false
        };
        [   'setFromAirportID',
            'setToAirportID',
            'setDepartDate',
            'setReturnDate',
            'flightSetupReady',
            'search'
        ].forEach( m => {
            this[m] = this[m].bind(this);
        });
    }
    setFromAirportID ( id ) {
        this.setState({
            fromAirportID: id
        });
    }
    setToAirportID ( id ) {
        this.setState({
            toAirportID: id
        });
    }
    setDepartDate ( e ) {
        this.setState({
            departDate: formatDate(e.target.value)
        });
    }
    setReturnDate ( e ) {
        this.setState({
            returnDate: formatDate(e.target.value)
        });
    }
    flightSetupReady () {
        return  this.state.fromAirportID !== null &&
                this.state.toAirportID !== null &&
                this.state.departDate &&
                ( this.state.oneWay ? true : this.state.returnDate )
                ;
    }
    search () {
        this.props.onSearch({
            from: this.state.fromAirportID,
            to: this.state.toAirportID,
            departDate: this.state.departDate,
            returnDate: this.state.returnDate
        });
    }
    render () {
        return (
            <div className="flight-selector">
                <div>
                    <label>From:</label>
                    <AirportSelector onAirportSelection={ this.setFromAirportID } />
                </div>
                <div>
                    <label>To:</label>
                    <AirportSelector onAirportSelection={ this.setToAirportID } />
                </div>
                <div>
                    <label>Depart:</label>
                    <input type="date" className="date-input" value={ formatDateForInput(this.state.departDate) } onChange={ this.setDepartDate }/>
                </div>
                { ! this.state.oneWay && (
                <div>
                    <label>Return:</label>
                    <input type="date" className="date-input" onChange={ this.setReturnDate } />
                </div>
                )}
                <div>
                    <button className="search-button" disabled={ ! this.flightSetupReady() } onClick={ this.search }>Search</button>
                </div>
            </div>
        );
    }
}

export default FlightSelector;
