import React, { Component } from 'react';
import './AirportSelector.css';
import API from '../io/api.js';
import debounce from '../utils/debounce.js';

let api = API.getInstance();

class AirportSelector extends Component {
    constructor ( props ) {
        super(props);
        this.state = {
            airportInput: "",
            places: [],
            selectedAirportID: null
        };
        this.findPlace = debounce(this.findPlace, 500);
    }
    userTyping ( text ) {
        this.setState({
            airportInput: text
        });
        if ( text && text.length ) {
            this.findPlace( text );
        } else {
            this.setState({
                places: []
            });
            this.props.onAirportSelection(null);
        }
    }
    findPlace ( text ) {
        api.findPlaces(text).then( places => {
            this.setState({
                places: places
            });
        });
    }
    selectPlace ( place ) {
        this.setState({
            airportInput: place.value,
            selectedAirportID: place.id,
            places: []
        });
        this.props.onAirportSelection(place.id);
    }
    render () {
        return (
            <div className="airport-selector">
                <input type="search" className="airport-input" value={ this.state.airportInput } placeholder="" onChange={ e => this.userTyping(e.target.value) } />
                <ul className={`airports-list ${ this.state.places.length ? "" : "hidden" }`}>
                    { this.state.places.map( place => {
                        return <li className="airport-list-item" key={place.id} onClick={ e => this.selectPlace(place) }>{ place.value }</li>;
                    })}
                </ul>
            </div>
        );
    }
}

export default AirportSelector;
