// Not in use
export default class RequestController {

    constructor ( api, control = [] ) {
        this.api = api;
        this.service =  {};
        control.forEach( serviceName => {
            this.service[serviceName] = 0;
        });
    }

    run ( serviceName, ...params ) {
        let controlled = this.service.hasOwnProperty(serviceName),
            valid = this.api.hasOwnProperty(serviceName);
        if ( valid ) {
            this.service[serviceName]++
            let reqID = this.service[serviceName];
            return new Promise(( resolve, reject ) => {
                this.api[serviceName].apply(null, params)
                .then(( ( reqID, serviceName ) => {
                    return data => {
                        let lastReqID = this.service[serviceName];
                        if ( !controlled || ( reqID === lastReqID ) ) {
                            resolve(data);
                        } else {
                            reject(new Error('canceled'));
                        }
                    };
                })( reqID, serviceName ))
                .catch( e => {
                    reject(new Error('network'));
                });
            });
        } else {
            return Promise.reject(new Error('service_not_found'));
        }
    }

}
