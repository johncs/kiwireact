/*
*   Makes a module a "Lazy initializated" Singleton.
*/
let Singleton = ( module, ...dependencies ) => {
    let instance = null;
    return {
        getInstance () {
            return instance || ( instance = module.apply( null, dependencies ) );
        }
    };
};

export default Singleton;
