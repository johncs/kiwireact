let debounce = ( callback, wait = 0 ) => {
    let timeout = null;
    return function ( ...args ) {
        clearTimeout( timeout );
        timeout = setTimeout( () => {
            callback.apply( this, args );
        }, wait );
    };
};

export default debounce;
