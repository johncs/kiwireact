export let formatDate = ( date ) => {
    return date.split('-').reverse().join('/');
};

export let formatDateForInput = date => {
    return date.split('/').reverse().join('-');
};

export let getToday = () => {
    let date = new Date(),
        year = date.getFullYear(),
        month = date.getMonth(),
        day = date.getDate();
    return `${ year }-${ month }-${ day }`;
};

export let createDateTime = ( timestamp ) => {
    let date = new Date(timestamp*1000),
        monthsNames = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        year = date.getFullYear(),
        month = monthsNames[date.getMonth()],
        day = date.getDate(),
        hours = date.getHours(),
        minutes = ("0" + date.getMinutes()).substr(-2);
    return `${ day } ${ month } ${ year }, ${ hours }:${ minutes}`;
};
