import React, { Component } from 'react';
import './Loader.css';

class Loader extends Component {
    constructor ( props ) {
        super(props);
        this.state = {

        };
        [].forEach( m => {
            this[m] = this[m].bind(this);
        });
    }
    render () {
        return (
            <div className="loader">
                <div className="spinner"></div>
            </div>
        );
    }
}

export default Loader;
