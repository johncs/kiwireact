import ajax from './ajax.js';
import Singleton from '../utils/Singleton.js';

let API = Singleton(( ajax ) => {

    const baseURL = 'https://api.skypicker.com';
    const api_version = '2';
    const locale = 'en';

    /* Generic request handler */
    let request = ({ url = '', method = 'get', params = {}, data = {} }) => {
        url = baseURL + url;
        Object.assign(params, { api_version, locale });
        return ajax({ url, method, params, data });
    };

    let findPlaces = ( term ) => {
        let params = { term };
        return request({ url: '/places', params });
    };

    let findFlights = ({ from, to, departDate, returnDate }) => {
        let params = {
            flyFrom: from,
            to: to,
            dateFrom: departDate
        };
        if ( returnDate ) {
            Object.assign(params, {
                typeFlight: 'return',
                returnFrom: returnDate,
                returnTo: returnDate
            })
        }
        return request({ url: '/flights', params });
    };

    return { findPlaces, findFlights };

}, ajax );

export default API;
