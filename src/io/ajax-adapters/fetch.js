/*
*   Adapter for standard fetch
*/
let fetchAdapter = ({ method, url, params, data }) => {
    let setup = { method, body: data },
        esc = encodeURIComponent;
        url += ( '?' + Object.keys(params)
                .map( k => esc(k) + '=' + esc(params[k]) )
                .join('&') );
    if ( method === 'get' || method === 'head' ) {
        delete setup.body;
    }
    return fetch( url, setup ).then( data => data.json() );
};

export default fetchAdapter;
