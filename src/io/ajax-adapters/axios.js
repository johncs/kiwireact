/*
*   Adapter for axios
*/
import axios from 'axios';

let axiosAdapter = ({ method, url, params, data }) => {
    return axios({ url, method, params, data }).then( data => data.data );
};

export default axiosAdapter;
