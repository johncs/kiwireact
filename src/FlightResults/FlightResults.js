import React, { Component } from 'react';
import './FlightResults.css';
import FlightItem from '../FlightItem/FlightItem.js';

class FlightResults extends Component {
    constructor ( props ) {
        super(props);
        this.state = {

        };
        [].forEach( m => {
            this[m] = this[m].bind(this);
        });
    }
    render () {
        const flightsList = (
            <ul className="flights-results">
                { this.props.flights.map( flight => { return (
                    <li key={ flight.id }>
                        <FlightItem data={ flight } />
                    </li>
                );})}
            </ul>
        );
        const noFlights = (
            <div className="no-results">No results</div>
        );
        return this.props.flights.length ? flightsList : noFlights;
    }
}

export default FlightResults;
